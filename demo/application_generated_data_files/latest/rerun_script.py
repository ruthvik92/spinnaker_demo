"""
general reload script - note that imports are required so don't remove them!
"""

# pacman imports
from pacman.model.placements.placements import Placements
from pacman.model.placements.placement import Placement
from pacman.model.routing_info.routing_info import RoutingInfo
from pacman.model.routing_info.subedge_routing_info import SubedgeRoutingInfo
from pacman.model.routing_tables.multicast_routing_tables import \
    MulticastRoutingTables
from pacman.model.tags.tags import Tags

# spinnman imports
from spinnman.model.core_subsets import CoreSubsets
from spinnman.model.core_subset import CoreSubset

# spinnmachine imports
from spinn_machine.tags.iptag import IPTag
from spinn_machine.tags.reverse_iptag import ReverseIPTag

# front end common imports
from spinn_front_end_common.utilities.report_states import ReportState
from spinn_front_end_common.utilities.reload.reload import Reload
from spinn_front_end_common.utilities.reload.reload_application_data \
    import ReloadApplicationData
from spinn_front_end_common.utilities.executable_targets \
    import ExecutableTargets
from spinn_front_end_common.utilities.reload.reload_routing_table import \
    ReloadRoutingTable
from spinn_front_end_common.utilities.reload.reload_buffered_vertex import \
    ReloadBufferedVertex
from spinn_front_end_common.utilities.notification_protocol.\
    socket_address import SocketAddress

# general imports
import logging
import os

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
for handler in logging.root.handlers:
    handler.setFormatter(logging.Formatter(
        fmt="%(asctime)-15s %(levelname)s: %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S"))

application_data = list()
binaries = ExecutableTargets()
iptags = list()
reverse_iptags = list()
buffered_tags = Tags()
buffered_placements = Placements()

routing_tables = MulticastRoutingTables()
# database params
socket_addresses = list()

reports_states = ReportState(False, False, False, False, False,
                             False, False, False, False, False)
machine_name = "192.168.240.1"
machine_version = 5
bmp_details = "None"
down_chips = "None"
down_cores = "None"
number_of_boards = 1
height = None
width = None
auto_detect_bmp = False
enable_reinjection = True
iptags.append(
    IPTag("192.168.240.1", 0, "0.0.0.0", 17896, True)) 
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_1_0_3.dat",
    1, 0, 3, 1612972032))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_0_0_9.dat",
    0, 0, 9, 1612972032))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_0_0_8.dat",
    0, 0, 8, 1612975920))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_1_0_1.dat",
    1, 0, 1, 1612977280))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_1_1_1.dat",
    1, 1, 1, 1612972032))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_1_0_6.dat",
    1, 0, 6, 1612982528))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_1_0_7.dat",
    1, 0, 7, 1612987776))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_0_0_7.dat",
    0, 0, 7, 1612981168))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_1_0_4.dat",
    1, 0, 4, 1612993024))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_0_0_6.dat",
    0, 0, 6, 1612986416))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_1_0_5.dat",
    1, 0, 5, 1612998272))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_0_0_5.dat",
    0, 0, 5, 1612986816))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_0_0_4.dat",
    0, 0, 4, 1613061952))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_0_0_3.dat",
    0, 0, 3, 1613221116))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_1_0_10.dat",
    1, 0, 10, 1613002160))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_0_0_16.dat",
    0, 0, 16, 1613417200))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_0_0_2.dat",
    0, 0, 2, 1613422448))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_1_0_11.dat",
    1, 0, 11, 1613007408))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_0_0_1.dat",
    0, 0, 1, 1613581668))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_1_0_8.dat",
    1, 0, 8, 1613011296))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_1_0_9.dat",
    1, 0, 9, 1613015184))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_0_0_15.dat",
    0, 0, 15, 1613602788))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_1_0_14.dat",
    1, 0, 14, 1613020432))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_0_0_14.dat",
    0, 0, 14, 1613606676))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_1_0_15.dat",
    1, 0, 15, 1613024320))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_0_0_13.dat",
    0, 0, 13, 1613611924))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_1_0_12.dat",
    1, 0, 12, 1613029568))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_0_0_12.dat",
    0, 0, 12, 1613617172))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_1_0_13.dat",
    1, 0, 13, 1613034816))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_0_0_11.dat",
    0, 0, 11, 1613621060))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_1_0_16.dat",
    1, 0, 16, 1613040064))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_1_0_2.dat",
    1, 0, 2, 1613045312))
application_data.append(ReloadApplicationData(
    "192.168.240.1_appData_0_0_10.dat",
    0, 0, 10, 1613626308))
reload_routing_table = ReloadRoutingTable()
routing_tables.add_routing_table(reload_routing_table.reload("picked_routing_table_for_0_0"))
reload_routing_table = ReloadRoutingTable()
routing_tables.add_routing_table(reload_routing_table.reload("picked_routing_table_for_1_1"))
reload_routing_table = ReloadRoutingTable()
routing_tables.add_routing_table(reload_routing_table.reload("picked_routing_table_for_1_0"))
binaries.add_subsets("/home/ruthvik/Desktop/sPyNNaker-2015.005.01/spynnaker/pyNN/model_binaries/IF_curr_exp.aplx", CoreSubsets([CoreSubset(0, 0, [2, 3, 4, 6, ]),]))
binaries.add_subsets("/home/ruthvik/Desktop/sPyNNaker-2015.005.01/spynnaker/pyNN/model_binaries/spike_source_poisson.aplx", CoreSubsets([CoreSubset(1, 0, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, ]),CoreSubset(0, 0, [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, ]),CoreSubset(1, 1, [1, ]),]))
binaries.add_subsets("/usr/local/lib/python2.7/dist-packages/spinn_front_end_common/common_model_binaries/reverse_iptag_multicast_source.aplx", CoreSubsets([CoreSubset(0, 0, [1, ]),]))
binaries.add_subsets("/home/ruthvik/Desktop/sPyNNaker-2015.005.01/spynnaker/pyNN/model_binaries/IF_curr_exp_stdp_mad_pair_additive.aplx", CoreSubsets([CoreSubset(0, 0, [5, ]),]))
vertex = ReloadBufferedVertex("Population 0:0:699", [(2, "Population 0_0_699_2", 1048576) ])
buffered_placements.add_placement(Placement(vertex, 0, 0, 1))
buffered_tags.add_ip_tag(IPTag("192.168.240.1", 0, "0.0.0.0", 17896, True), vertex) 

reloader = Reload(machine_name, machine_version, reports_states, bmp_details, down_chips, down_cores, number_of_boards, height, width, auto_detect_bmp,enable_reinjection)
if len(socket_addresses) > 0:
    reloader.execute_notification_protocol_read_messages(socket_addresses, None, os.path.join(os.path.dirname(os.path.abspath(__file__)), "input_output_database.db"))
reloader.reload_application_data(application_data)
reloader.reload_routes(routing_tables)
reloader.reload_tags(iptags, reverse_iptags)
reloader.reload_binaries(binaries)
reloader.enable_buffer_manager(buffered_placements, buffered_tags)
reloader.restart(binaries, 3000, 1, turn_off_machine=True)
